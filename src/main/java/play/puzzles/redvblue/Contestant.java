package play.puzzles.redvblue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Michael on 8/26/2016.
 */
public class Contestant implements Comparable<Contestant> {
    private static Logger logger = LoggerFactory.getLogger(Contestant.class);

    private Integer position;
    private Integer blueHatTally = 0;
    private Integer redHatTally = 0;
    private LastTally lastTally;
    private Integer numberOfRedsAhead;
    private Status status = Status.LIVING;
    private EvenOdd evenOdd = EvenOdd.EVEN; // Default determined by everyone that even will be the starting assumption

    public Contestant(Integer position, Integer hatCount) {
        this.position = position;
        this.numberOfRedsAhead = hatCount;
    }

    public Integer getPosition() {
        return position;
    }

    public Integer getNumberOfRedsAhead() {
        return numberOfRedsAhead;
    }

    public void setNumberOfRedsAhead(Integer numberOfRedsAhead) {
        this.numberOfRedsAhead = numberOfRedsAhead;
    }

    /**
     * Answer the question when asked "What color is your hat?"
     *
     * @return Hat color we believe we have on.
     */
    public HatColor declareHatColor() {

        HatColor retval;

        // Determine the redModulus by summing what we know about Red and dividing by two
        // effectively giving us an even/odd value if redModulus == 0 even count, else odd
        // count.
        int redModulus = (numberOfRedsAhead + redHatTally) % 2;

        if (EvenOdd.ODD == evenOdd) {
            retval = redModulus > 0 ? HatColor.BLUE : HatColor.RED;

        } else { // evens by default
            retval = redModulus == 0 ? HatColor.BLUE : HatColor.RED;
        }


        logger.trace("Calling out that I am a {} hat!", retval);
        return retval;
    }

    public void getShot() {
        this.status = Status.DEAD;
    }

    /**
     * What to do when we hear an answer.
     *
     * @param hatColor
     */
    public void hearHatColor(HatColor hatColor) {

        if (HatColor.RED == hatColor) {
            this.redHatTally++;
            this.lastTally = LastTally.RED;
        } else {
            this.blueHatTally++;
            this.lastTally = LastTally.BLUE;
        }
    }

    /**
     * Work on the assumption we hear the answer first then the gunshot.  If we hear a gunshot, the previous answer
     * given was wrong.
     */
    public void hearGunShot() {

        // If we have just heard the first person in line get shot change our hat state
        if (redHatTally + blueHatTally == 1) {
            // Looks like our original thought of even number hat colors was wrong, 1st person died
            evenOdd = EvenOdd.ODD;
        }

        if (LastTally.RED == lastTally) {
            logger.trace("Gunshot heard, subtracting from Red and adding to Blue.");
            this.redHatTally--;
            this.blueHatTally++;
        } else {
            logger.trace("Gunshot heard, subtracting from Blue and adding to Red.");
            this.blueHatTally--;
            this.redHatTally++;
        }
    }

    public enum Status {
        LIVING, DEAD;
    }

    private enum LastTally {
        RED, BLUE;
    }

    private enum EvenOdd {
        EVEN, ODD;
    }

    @Override
    public String toString() {
        return String.format("Contestant %s knows %s red hats in front of them and is %s.", this.position + 1, this.numberOfRedsAhead, this.status).toString();
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     * <p>
     * <p>The implementor must ensure <tt>sgn(x.compareTo(y)) ==
     * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
     * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
     * <tt>y.compareTo(x)</tt> throws an exception.)
     * <p>
     * <p>The implementor must also ensure that the relation is transitive:
     * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
     * <tt>x.compareTo(z)&gt;0</tt>.
     * <p>
     * <p>Finally, the implementor must ensure that <tt>x.compareTo(y)==0</tt>
     * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
     * all <tt>z</tt>.
     * <p>
     * <p>It is strongly recommended, but <i>not</i> strictly required that
     * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
     * class that implements the <tt>Comparable</tt> interface and violates
     * this condition should clearly indicate this fact.  The recommended
     * language is "Note: this class has a natural ordering that is
     * inconsistent with equals."
     * <p>
     * <p>In the foregoing description, the notation
     * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
     * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
     * <tt>0</tt>, or <tt>1</tt> according to whether the value of
     * <i>expression</i> is negative, zero or positive.
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than
     * the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it from being compared to this object.
     */
    public int compareTo(Contestant o) {
        // We want our list to appear in descending order as the game makers want to start at the 100th person.
        if (this.getPosition() == o.getPosition()) {
            return 0; // objects are equal in position, shouldn't happen
        } else if (this.getPosition() > o.getPosition()) {
            return -1; // this object position is greater than the supplied so we want to land below it
        } else {
            return 1; // this object position is les than the supplied object so we want to land above it
        }
    }
}
