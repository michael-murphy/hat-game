package play.puzzles.redvblue;

/**
 * Created by Michael on 8/26/2016.
 */
public enum HatColor {

    RED, BLUE;
}
