package play.puzzles.redvblue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
// @formatter:off
/**
 * Contestant planning:
 * </p>
 * <ol>
 *     <li>Contestants will all assume there are an even number of red and blue hats.</li>
 *     <li>If a gun shot is heard, they will change their position to an odd number of red and blue hats.</li>
 *     <li>Contestants wil tally up the hats in front of them with what they have heard then choose their color hat based on the even/odd number hats.</li>
 *     <li>It is assumed that the first person asked has a 50/50 chance of survival as they do not know even or odd with certainty.</li>
 *     <li>All contestants must play by the rules or many will die.</li>
 * </ol>
 * @author Michael Murphy (michael.murphy.1300@gmail.com)
 */
// @formatter:on
public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    private List<HatColor> hatColors = new ArrayList<HatColor>();
    private Map<Contestant, HatColor> hatColorMap = new TreeMap<Contestant, HatColor>();

    public static void main(String args[]) {

        int results = new Application().run(100);

        if (results > 1) {
            // if > 1 then our basic logic has failed us miserably, we know 1 is acceptable.
            System.exit(1);
        } else {
            System.exit(0);
        }
    }

    /**
     * Runs the very morbid game of lining up people and shooting them if they don't answer correctly to the color of
     * the hat they are wearing.  Which, by the way, they cannot see.  What a sick game, right?
     *
     * @param numberOfContestants - how many people will we pick on today?
     * @return - Number of contestants shot by the game makers... Sounds almost like the Hunger Games anyone?
     */
    public int run(int numberOfContestants) {
        // Setup contestants
        loadContestants(numberOfContestants);

        int shotsFired = 0;
        for (Map.Entry<Contestant, HatColor> entry : hatColorMap.entrySet()) {
            HatColor answer = entry.getKey().declareHatColor();

            // All contestants hear answer
            for (Contestant witness : hatColorMap.keySet()) {
                if (witness.getPosition() < entry.getKey().getPosition()) {
                    witness.hearHatColor(answer);
                }
            }

            // If answers don't match, fire the gun...
            if (entry.getValue() != answer) {
                entry.getKey().getShot();
                logger.debug("{} called out {}", entry.getKey(), answer);
                shotsFired++;
                for (Contestant witness : hatColorMap.keySet()) {
                    witness.hearGunShot();
                }
            }
            logger.trace(entry.getKey().toString());
        }

        logger.info("{} contestant(s) shot.", shotsFired);
        return shotsFired;
    }

    /**
     * Create the list of contestants based on the numberOfContestants wanted with randomly generated {@link
     * HatColor}s.
     *
     * @param numberOfContestants - the number of {@link Contestant}s to be returned
     * @return
     */
    private void loadContestants(Integer numberOfContestants) {
        Random random = new Random();
        int redHatCount = 0;
        HatColor hatColor;

        for (int i = 0; i < numberOfContestants; i++) {
            // Create the contestant first so they get the correct redHatCount
            Contestant c = new Contestant(i, redHatCount);

            // randomly set the hat to our poor contestant, but don't let them know it.
            if ((random.nextInt(533) % 2) == 0) {
                hatColor = HatColor.RED;
                redHatCount++;
            } else {
                hatColor = HatColor.BLUE;
            }
            logger.trace("New {} hat Contestant {}", hatColor, c);

            // map our contestants and their hats for lookup during the game.
            hatColorMap.put(c, hatColor);
        }

        logger.info("{} contestants setup with {} red hats and {} blue hats", numberOfContestants, redHatCount, numberOfContestants - redHatCount);
    }


}
